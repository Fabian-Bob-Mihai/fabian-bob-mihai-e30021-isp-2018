package person;

public class Person {

    String Nume;
    int varsta;

    public Person(int varsta, String Nume) {
        this.varsta = varsta;
        this.Nume = Nume;
    }

    public Person() {
        Nume = "Ion";
        varsta = 20;
    }

    int gVarsta() {
        return this.varsta;
    }

    String gNume() {
        return this.Nume;
    }

    public String Strin() {
        return "Numele persoanei este:" + this.Nume + " si varsta lui este:" + gVarsta();
    }

    public static void main(String[] args) {
        Person Ion = new Person();
        System.out.println(Ion.Strin());
        Person n = new Professor(11, "Mihai", 30);
        System.out.println(n.Strin());
        Person m=new Student(7,"Stannis",27);
        System.out.println(m.Strin());

    }

}
