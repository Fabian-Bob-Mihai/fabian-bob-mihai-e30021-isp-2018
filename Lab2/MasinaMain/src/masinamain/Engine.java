
package masinamain;

public class Engine {
    String fuellType;
            String distribution;
            long capacity;
            boolean active;
 
            Engine(int capacity,boolean active){
                        this.capacity = capacity;
                        this.active = active;
            }          
            Engine(int capacity,boolean active,String fuellType,String distribution){
                        this(capacity,active);
                        this.fuellType = fuellType;
                        this.distribution=distribution;
            }          
            Engine(){
                        this(2500,false,"diesel","strap");
            }          
            void print(){
                        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active+"distribution="+distribution);
           }
            public static void main(String[] args) {
                        Engine tdi = new Engine();
                        Engine i16 = new Engine(1500,false,"petrol","strap");
                        Engine d30 = new Engine(3500,true,"diesel","strap");
                       tdi.print();i16.print();d30.print();
          }

}
