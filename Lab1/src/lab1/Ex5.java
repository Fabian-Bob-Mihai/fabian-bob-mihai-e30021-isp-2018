package lab1;

import java.util.Scanner;
import java.util.Random;

public class Ex5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        int i;
        int j;
        int schimb;
        Random r = new Random();
        int[] v = new int[10];
        for (i = 0; i < v.length; i++) {
            v[i] = r.nextInt(1000);
            System.out.print("v[" + i + "]=" + v[i] + " ");
        }
        for (i = 0; i < 9; i++) {
            for (j = 1; j < (10 - i); j++) {
                if (v[j - 1] > v[j]) {
                    schimb = v[j];
                    v[j] = v[j - 1];
                    v[j - 1] = schimb;
                }
            }
        }
        System.out.println("Vectorul sortat este:");
        for (i = 0; i <= 9; i++) {
            System.out.println(v[i]);
        }

    }
}
